package com.example.rushhour.fragments;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.navigation.fragment.NavHostFragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.rushhour.R;
import com.example.rushhour.databinding.FragmentGameViewBinding;
import com.example.rushhour.databinding.FragmentMenuBinding;

public class GameViewFragment extends Fragment {
    private FragmentGameViewBinding gameViewBinding;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        gameViewBinding = FragmentGameViewBinding.inflate(inflater, container, false);
        return gameViewBinding.getRoot();
    }
    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        gameViewBinding.back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NavHostFragment.findNavController(GameViewFragment.this)
                        .navigate(R.id.action_gameViewFragment_to_MenuFragment);
            }
        });
    }
}